angular.module("starter.services", [])
.factory('UserServiceProvincias', function ($resource) {
	var datos = [];
	datos = $resource('http://acontecimientos.esy.es/rest/ubi', {}, {get:{method:'GET', isArray:true}}); 
    return datos;
})
.factory('UserServiceTipos', function ($resource) {
	var datos = [];
	datos = $resource('http://acontecimientos.esy.es/rest/search/:id',{id: "@id"}, {get:{method:'GET', isArray:true}}); 
    return datos;
});